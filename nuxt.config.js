module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'invoice_frontend',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt application for generating PDF invoices' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  modules: [
    'nuxt-buefy',
  ],
  /**
   * Custom css
   */
  css:[
    { src: '~/assets/styles.sass', lang: 'sass' },
  ],
  /**
   * Environment variables
   */
  env: {
    api: 'http://127.0.0.1:8000'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
