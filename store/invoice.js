import axios from 'axios/index';

export const state = () => ({
});

export const mutations = {

}

export const getters = {

}

export const actions = {
  generate({commit, dispatch}, data) {
    const url = this.app.context.env.api;
    axios.post(`${url}/invoice`, data, {
      responseType: 'arraybuffer'
    }).then((res) => {
      console.log(res);
      let filename = res.headers['content-disposition'];
      if (filename) {
        filename = filename.split(';')[1].split('=')[1]
      } else {
        filename = 'file.pdf'
      }
      const url = window.URL.createObjectURL(new Blob([res.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', filename);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }).catch(({response}) => {
      console.log(response);
    });
  }
}
