FROM node:10.4.1-alpine
MAINTAINER michal@vane.pl
WORKDIR /
RUN apk --no-cache add bash
COPY . /invoice_frontend
WORKDIR /invoice_frontend
EXPOSE 3000
CMD yarn